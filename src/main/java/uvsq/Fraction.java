/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uvsq;

/**
 *
 * @author User
 */
public class Fraction {
    private int num;
    private int den;
    
    public Fraction (int num,int den){
    	this.num=num ;
    	this.den=den ;
    }
    public Fraction (int num) {
    	this.num=num;
    	den=1;
    }
    public Fraction () {
    	this.num=0;
    	this.den=1;
    }
     //getteurs //
    public int getNum() {
    	return num;
    }
    public int getDen() {
    	return den;
    }
    

    public String toString () {
    	 return "le numerateur :"+num+ "et le denominateur "+den;
    }
    
    public double consultation () {
    	return (double)num / (double)den ;
    }
      public String conversion (){
    	return Integer.toString(num)+" "+Integer.toString(den);

    }
}
