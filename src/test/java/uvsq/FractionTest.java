/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uvsq;

import junit.framework.TestCase;

/**
 *
 * @author User
 */
public class FractionTest extends TestCase {
    
    public FractionTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testSomeMethod() {
        // TODO review the generated test code and remove the default call to fail.

        //fail("The test case is a prototype.");
    }

    /**
     * Test of getNum method, of class Fraction.
     */
    public void testGetNum() {
        System.out.println("getNum");
        Fraction instance = new Fraction(4,5);
        int expResult = 4;
        int result = instance.getNum();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDen method, of class Fraction.
     */
    public void testGetDen() {
        System.out.println("getDen");
        Fraction instance = new Fraction(4,5);
        int expResult = 5;
        int result = instance.getDen();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class Fraction.
     */
    public void testToString() {
        System.out.println("toString");
        Fraction instance = new Fraction(5,4);
        String expResult = "le numerateur :"+5+ "et le denominateur "+4;
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of consultation method, of class Fraction.
     */
    public void testConsultation() {
        System.out.println("consultation");
        Fraction instance = new Fraction(10,2);
        double expResult = 5.0;
        double result = instance.consultation();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.

       // fail("The test case is a prototype.");
    }

    /**
     * Test of conversion method, of class Fraction.
     */
    public void testConversion() {
        System.out.println("conversion");
        Fraction instance = new Fraction(4,5);
        String expResult = "4 5";
        String result = instance.conversion();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

       // fail("The test case is a prototype.");
    }
    
}
